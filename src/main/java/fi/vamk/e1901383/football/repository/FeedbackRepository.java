package fi.vamk.e1901383.football.repository;

import org.springframework.data.repository.CrudRepository;

import fi.vamk.e1901383.football.entity.Feedback;

public interface FeedbackRepository extends CrudRepository<Feedback, Integer>{
	
}
