package fi.vamk.e1901383.football.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fi.vamk.e1901383.football.entity.Game;

public interface GameRepository extends JpaRepository<Game, Integer>{

	List<Game> findByOrderByMatchnumber();

	List<Game> findByHometeamOrAwayteam(String homeTeam, String awayTeam);

	List<Game> findByLeaguecode(String leaguecode);

	List<Game> findBySeasonOrderByMatchnumber(String season);

}
