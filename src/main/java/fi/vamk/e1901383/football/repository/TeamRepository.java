package fi.vamk.e1901383.football.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fi.vamk.e1901383.football.entity.Team;

public interface TeamRepository extends JpaRepository<Team, Integer>{

	List<Team> findByOrderByTeamname();

}
