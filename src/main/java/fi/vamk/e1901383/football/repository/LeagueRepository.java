package fi.vamk.e1901383.football.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fi.vamk.e1901383.football.entity.League;

public interface LeagueRepository extends CrudRepository<League, Integer>{

	List<League> findByOrderByLeaguename();

}
