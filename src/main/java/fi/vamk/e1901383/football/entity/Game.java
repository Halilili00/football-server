package fi.vamk.e1901383.football.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int matchnumber;
    private int roundnumber;
    private String date;
    private String location;
    private String hometeam;
    private String awayteam;
    private int hometeamscore;
    private int awayteamscore;
    private String season;
    private String leaguecode;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMatchnumber() {
		return matchnumber;
	}
	public void setMatchnumber(int matchnumber) {
		this.matchnumber = matchnumber;
	}
	public int getRoundnumber() {
		return roundnumber;
	}
	public void setRoundnumber(int roundnumber) {
		this.roundnumber = roundnumber;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getHometeam() {
		return hometeam;
	}
	public void setHometeam(String hometeam) {
		this.hometeam = hometeam;
	}
	public String getAwayteam() {
		return awayteam;
	}
	public void setAwayteam(String awayteam) {
		this.awayteam = awayteam;
	}
	public int getHometeamscore() {
		return hometeamscore;
	}
	public void setHometeamscore(int hometeamscore) {
		this.hometeamscore = hometeamscore;
	}
	public int getAwayteamscore() {
		return awayteamscore;
	}
	public void setAwayteamscore(int awayteamscore) {
		this.awayteamscore = awayteamscore;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getLeaguecode() {
		return leaguecode;
	}
	public void setLeaguecode(String leaguecode) {
		this.leaguecode = leaguecode;
	}
    
}
