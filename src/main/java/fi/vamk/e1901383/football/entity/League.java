package fi.vamk.e1901383.football.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class League {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String leaguename;
	private String leaguecode;
	
	public League() {};

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLeaguename() {
		return leaguename;
	}

	public void setLeaguename(String leaguename) {
		this.leaguename = leaguename;
	}

	public String getLeaguecode() {
		return leaguecode;
	}

	public void setLeaguecode(String leaguecode) {
		this.leaguecode = leaguecode;
	}
	
	
	
}