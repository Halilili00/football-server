package fi.vamk.e1901383.football.controller;

import java.io.FileReader;
import java.util.Iterator;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import fi.vamk.e1901383.football.entity.Game;
import fi.vamk.e1901383.football.entity.League;
import fi.vamk.e1901383.football.repository.LeagueRepository;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LeagueController {
	@Autowired
	LeagueRepository repository;
	
	@GetMapping("/importLeagues")
	public String importLeagues() {
		repository.deleteAll();
		JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("leagues.json"));
            // muunnetaan tiedosto JSON arrayksi
            JSONArray leagues = (JSONArray) obj;
            Iterator<JSONObject> iterator = leagues.iterator();
            while (iterator.hasNext()) {
                JSONObject league = (JSONObject) iterator.next();
                // System.out.println(leagues.toString());
                ObjectMapper mapper = new ObjectMapper();
                League b = mapper.readValue(league.toString(), League.class);
                repository.save(b);
            }
            return "Import successful";
        } catch (Exception e) {
            return "Import failed " + e.toString();
        }
	}
	
	@CrossOrigin( origins = {"https://football383.herokuapp.com", "http://localhost:3000"})
	@GetMapping("/leagues")
    public Iterable<League> getLeagues() {
    	return repository.findByOrderByLeaguename();
    }
}
