package fi.vamk.e1901383.football.controller;

import java.io.FileReader;
import java.util.Iterator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fi.vamk.e1901383.football.entity.Game;
import fi.vamk.e1901383.football.repository.GameRepository;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameController {
	@Autowired
    GameRepository repository;
	
	@GetMapping("/importMatches")
	public String importMatches() {
		repository.deleteAll();
		JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("pl2021.json"));
            // muunnetaan tiedosto JSON arrayksi
            JSONArray matches = (JSONArray) obj;
            Iterator<JSONObject> iterator = matches.iterator();
            while (iterator.hasNext()) {
                JSONObject match = (JSONObject) iterator.next();
                // System.out.println(match.toString());
                ObjectMapper mapper = new ObjectMapper();
                Game b = mapper.readValue(match.toString(), Game.class);
                repository.save(b);
            }
            return "Import successful";
        } catch (Exception e) {
            return "Import failed " + e.toString();
        }
	}
	
	@CrossOrigin( origins = {"https://football383.herokuapp.com", "http://localhost:3000"})
	@GetMapping("/matches")
    public Iterable<Game> getMatches() {
    	return repository.findByOrderByMatchnumber();
    }
	
	@CrossOrigin( origins = {"https://football383.herokuapp.com", "http://localhost:3000"})
	@GetMapping("/matches/{hometeam}")
    public Iterable<Game> getMatchesWithTeamName(@PathVariable("hometeam") String teamName) {
    	return repository.findByHometeamOrAwayteam(teamName, teamName);
    }
	
	@CrossOrigin( origins = {"https://football383.herokuapp.com", "http://localhost:3000"})
	@GetMapping("/matches/league/{leaguecode}")
	public Iterable<Game> getMatchesWithLeagueCode(@PathVariable("leaguecode") String leagueCode){
		return repository.findByLeaguecode(leagueCode);
	}
	
	@CrossOrigin( origins = {"https://football383.herokuapp.com", "http://localhost:3000"})
	@GetMapping("/matches/season/{season}")
    public Iterable<Game> getMatchesWithSeason(@PathVariable("season") String season) {
    	return repository.findBySeasonOrderByMatchnumber(season);
    }

}

