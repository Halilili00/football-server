package fi.vamk.e1901383.football.controller;

import java.rmi.ServerException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fi.vamk.e1901383.football.entity.Feedback;
import fi.vamk.e1901383.football.repository.FeedbackRepository;

@RestController
public class FeedbackController {
	@Autowired
	private FeedbackRepository repository;
	
	@CrossOrigin( origins = {"https://football383.herokuapp.com", "http://localhost:3000"})
	@GetMapping("/feedbacks")
	public Iterable<Feedback> list(){
		return repository.findAll();
	}
	
	@CrossOrigin( origins = {"https://football383.herokuapp.com", "http://localhost:3000"})
	@PostMapping("feedbackpost")
	public @ResponseBody Feedback create(@RequestBody Feedback item) throws ServerException {
		return repository.save(item);
	}
	
	@CrossOrigin( origins = {"https://football383.herokuapp.com", "http://localhost:3000"})
	@DeleteMapping("/feedback")
	public void date(@RequestBody Feedback itme) {
		repository.delete(itme);
	}

	@DeleteMapping("/feedback/deleteAll")
	public void dateAll() {
		repository.deleteAll();
	}
	
	@GetMapping("/feedback/{id}")
	public Optional<Feedback> get(@PathVariable("id") int id){
		return repository.findById(id);
	}

}
