package fi.vamk.e1901383.football.controller;

import java.io.FileReader;
import java.util.Iterator;

import com.fasterxml.jackson.databind.ObjectMapper;

import fi.vamk.e1901383.football.entity.Team;
import fi.vamk.e1901383.football.repository.TeamRepository;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeamController {
	@Autowired
	TeamRepository repository;
	
	@GetMapping("/importTeams")
	public String importTeams() {
		repository.deleteAll();
		JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("teams.json"));
            // muunnetaan tiedosto JSON arrayksi
            JSONArray teams = (JSONArray) obj;
            Iterator<JSONObject> iterator = teams.iterator();
            while (iterator.hasNext()) {
                JSONObject team = (JSONObject) iterator.next();
                // System.out.println(teams.toString());
                ObjectMapper mapper = new ObjectMapper();
                Team b = mapper.readValue(team.toString(), Team.class);
                repository.save(b);
            }
            return "Import successful";
        } catch (Exception e) {
            return "Import failed " + e.toString();
        }
	}
	
	@CrossOrigin( origins = {"https://football383.herokuapp.com", "http://localhost:3000"})
	@GetMapping("/teams")
    public Iterable<Team> getTeams() {
    	return repository.findByOrderByTeamname();
    }
}
