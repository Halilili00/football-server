package fi.vamk.e1901383.football;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import fi.vamk.e1901383.football.entity.Feedback;
import fi.vamk.e1901383.football.repository.FeedbackRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = {"fi.vamk.e1901383.football"})
@EnableJpaRepositories(basePackageClasses = FeedbackRepository.class)
public class FeedbackTests {
	@Autowired
	private FeedbackRepository repository;
	
	LocalDateTime now = LocalDateTime.now();
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
	
	//Test to adding item to database
	@Test
	public void addFeedbackTest() throws ParseException {
		Feedback fb = new Feedback();
		fb.setUsername("Test1");
		fb.setComment("Test commment");
		fb.setTeamname("Arsenal");
		fb.setCommentdate(dtf.format(now));
		Feedback saved = repository.save(fb);
		Optional<Feedback> found = repository.findById(fb.getId());
		assertThat(found.get().toString()).isEqualTo(fb.toString());
		repository.delete(saved);
	}
	
	//Test to getting one from database based on id
	@Test
	public void getFeedbackWithIdTest() throws ParseException {
		Feedback fb = new Feedback();
		fb.setUsername("Test2");
		fb.setComment("Test commment");
		fb.setTeamname("Arsenal");
		fb.setCommentdate(dtf.format(now));
		Feedback saved = repository.save(fb);
		//Here I search with id
		//This test is same as addFeedbackTest, because we also there test the same think that
		//we try to get the feedback with id what we added in this test case
		Optional<Feedback> find = repository.findById(fb.getId());
		assertThat(find.get().toString()).isEqualTo(fb.toString());
		repository.delete(saved);
	}
	
	//Test to get all data and check that is the right amount of data in beginning and end
	@Test
	public void getAllFeedbacksTest() throws ParseException {
		Iterable<Feedback> begin = repository.findAll();
		Feedback fb = new Feedback();
		fb.setUsername("Test3");
		fb.setComment("Test commment");
		fb.setTeamname("Arsenal");
		fb.setCommentdate(dtf.format(now));
		Feedback saved = repository.save(fb);
		repository.delete(saved);
		Iterable<Feedback> end = repository.findAll();
		assertEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));
	}
}
