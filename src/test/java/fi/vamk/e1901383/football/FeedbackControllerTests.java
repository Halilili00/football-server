package fi.vamk.e1901383.football;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import fi.vamk.e1901383.football.entity.Feedback;
import fi.vamk.e1901383.football.repository.FeedbackRepository;

@SpringBootTest
@AutoConfigureMockMvc
public class FeedbackControllerTests extends AbstractTest{
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private FeedbackRepository repository;
	
	LocalDateTime now = LocalDateTime.now();
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
	
	//Test to RESTful API GET 
	@WithMockUser("USER")
	@Test
	public void getFeedback() throws Exception{
		Feedback fb = new Feedback();
		fb.setUsername("Test4");
		fb.setComment("Test commment");
		fb.setTeamname("Arsenal");
		fb.setCommentdate(dtf.format(now));
		fb = repository.save(fb);	
		Feedback[] arr = new Feedback[1];
		arr[0] = fb;
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/feedback/" + fb.getId())
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		assertEquals(super.mapToJson(fb), content);
		repository.delete(fb);
	}
	
	//Test to RESTful API POST 
	@WithMockUser("USER")
	@Test
	public void postFeedbackTest() throws Exception{
		Feedback fb = new Feedback();
		fb.setUsername("Test5");
		fb.setComment("Test commment");
		fb.setTeamname("Arsenal");
		fb.setCommentdate(dtf.format(now));
		fb = repository.save(fb);
		String inputJson = super.mapToJson(fb);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/feedbackpost")
			      .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		int status = mvcResult.getResponse().getStatus();
	    assertEquals(200, status);
	    String content = mvcResult.getResponse().getContentAsString();
	    assertEquals(super.mapToJson(fb), content);
	    repository.delete(fb);
	}
	
	//Get all feedbacks. Test to RESTful API GET
	@WithMockUser("USER")
	@Test
	public void getAllFeedbacks() throws Exception {
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/feedbacks")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Feedback[] feedbacks = super.mapFromJson(content, Feedback[].class);
		assertTrue(feedbacks.length > 0);
	}
	
	//Test to RESTful API Delete
	@WithMockUser("USER")
	@Test
	public void deleteFeedback() throws Exception {
		Feedback fb = new Feedback();
		fb.setUsername("Test6");
		fb.setComment("Test commment");
		fb.setTeamname("Arsenal");
		fb.setCommentdate(dtf.format(now));
		fb = repository.save(fb);
		String inputJson = super.mapToJson(fb);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete("/feedback")
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		assertEquals("", content);
	}
}
