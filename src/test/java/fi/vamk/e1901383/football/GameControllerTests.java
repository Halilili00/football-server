package fi.vamk.e1901383.football;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import fi.vamk.e1901383.football.entity.Game;

@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTests extends AbstractTest{
	@Autowired
	private MockMvc mvc;
	
	@WithMockUser("USER")
	@Test
	public void getGamesWithTeamName() throws Exception {
		String testTeam = "Arsenal";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/matches/" + testTeam)
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Game[] games = super.mapFromJson(content, Game[].class);
		assertTrue(games.length > 0);
		//here i count how many time testTeam occures in our getting results
		int lastIndex = 0;
		int occure = 0;
		while(lastIndex != -1){
		    lastIndex = content.indexOf(testTeam,lastIndex);
		    if(lastIndex != -1){
		    	occure ++;
		        lastIndex += testTeam.length();
		    }
		}
		//If getWithName functions work, that mean that occure === games.length
		assertEquals(occure, games.length);
	}
}
