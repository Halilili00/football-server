package fi.vamk.e1901383.football;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import fi.vamk.e1901383.football.entity.League;

@SpringBootTest
@AutoConfigureMockMvc
public class LeagueControllerTests extends AbstractTest{
	@Autowired
	private MockMvc mvc;
	
	@WithMockUser("USER")
	@Test
	public void getAllLeagues () throws Exception{
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/leagues")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		League[] leagues = super.mapFromJson(content, League[].class);
		assertEquals(5, leagues.length);
	}
}
