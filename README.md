# Football-server

This project is service for football383-client. Whit this project we create table and controll data with GETting, POSTing and Deleting.

## Requirements

- JDK 11
- Maven
- Spring MVC with Spring Boot
- Database postgresql

## How to start the app

You can start the app by executing https://football-server-383.herokuapp.com/swagger-ui.html, which start a webserver and serves SwaggerUI where can inspect and try exiting endpoints.
